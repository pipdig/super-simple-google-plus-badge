=== Google+ Badge Widget ===
Contributors: aaronjamesy
Tags: google+, google, google plus, badge, social, widget
Requires at least: 4.3
Tested up to: 4.5
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Adds a Google+ Badge Widget to your WordPress site so your readers can follow you on Google+!

== Description ==

This plugin adds a Google+ Badge Widget to your WordPress site.

All of the options given on the official Google+ badge site (https://developers.google.com/+/web/badge/) are given to you, so it's fully customisable!

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the `Plugins` screen in WordPress.
3. Enter your user ID in the widget's form and customise as needed.

== Changelog ==

= 1.0 =
* Initial version